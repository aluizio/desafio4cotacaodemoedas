# Project: Desafio 4 Cotação de Moedas

This project make requests to the Bacen data to get information about the cotation of the coins in a given day,
and then it looks for the coin with the least cotation of that day, for last it prints in the terminal the info of that coin.

## To execute this project run the bellow command with a given argument:

    ./requestCoinData.py YYYYMMDD

or

    python3 requestCoinData.py YYYYMMDD

## In some cases you may not find the library requests, so you can install the dependencies:

### Intall with pipenv

    pipenv install
    pipenv shell

### Or install directly

    pip3 install requests