#!/usr/bin/python3

import xml.etree.ElementTree as ET
import requests
from multiprocessing import Pool
import os



def loadXMLDataFromRequest(url):
    response = requests.get(url)

    if response.status_code == requests.codes.ok:
        data = ET.fromstring(response.content)
        return data

    return None


def loadCoinsData(url):
    xml_coins = loadXMLDataFromRequest(url)
    if xml_coins is None:
        raise ValueError("Não foi possível carregar os dados sobre as moedas!")
    coins={}
    for xml_coin in xml_coins:
        code = xml_coin.find('codigo').text
        coins[code] = {
            'countryCode': xml_coin.find('codigoPais').text,
            'swiftCode': xml_coin.find('codigoSwift').text,
            'name': xml_coin.find('nome').text
        }
    return coins


def get_task(task):
    value = loadXMLDataFromRequest(task[1])
    if value is not None:
        value = float(value.text)
    return (
        task[0],
        value
    )


def requestCheapestCoinFromDate(date):
    year = date[:4]
    month = date[4:6]
    day = date[6:8]

    base_conversion_url = 'https://www3.bcb.gov.br/bc_moeda/rest/converter/1/1/' # coin_code/220/data:2020-09-29'
    coins_url = 'https://www3.bcb.gov.br/bc_moeda/rest/moeda/data'

    coins = loadCoinsData(coins_url)
    dolar_code = '220'

    urls = []
    for code in coins:
        conversion_url = base_conversion_url + code + '/' + dolar_code + '/' + year + '-' + month + '-' + day
        urls.append((code,conversion_url))
    
    coinsConversion = []

    with Pool(processes=os.cpu_count()) as pool:
        results = pool.imap_unordered(get_task, urls)
        coinsConversion = list(filter(lambda x: x[1] is not None, results))


    if len(coinsConversion) == 0 or len(coinsConversion) == 1:
        print("x")
    else:
        coinsConversion.sort(key=lambda v: v[1])

        cheapestCoin = coinsConversion[0]
        coin = coins[cheapestCoin[0]]

        print(coin['swiftCode']+','+coin['name']+','+str(cheapestCoin[1]))


if __name__ == "__main__":
    import sys
    if len(sys.argv) == 1:
        print("Passe uma data no formato YYYYMMDD")
    else:
        date = sys.argv[1]
        requestCheapestCoinFromDate(date)
